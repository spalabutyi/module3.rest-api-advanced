package com.epam.esm.application.exeption;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorResponse {

    private final String errorMessage;
    private final long errorCode;

    public ErrorResponse(String errorMessage, Errors errorCode) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode.getErrorCode();
    }
}
