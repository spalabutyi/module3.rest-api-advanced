package com.epam.esm.application.handler;

import com.epam.esm.application.exeption.ErrorResponse;
import com.epam.esm.application.exeption.Errors;
import com.epam.esm.repository.exception.*;
import com.epam.esm.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.stream.Collectors;

@ControllerAdvice
@PropertySource("classpath:errormessages.properties")
public class ControllerExceptionHandler {

    private final Environment env;

    @Autowired
    public ControllerExceptionHandler(Environment env) {
        this.env = env;
    }

    /*
     * Tag exceptions
     */
    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleTagNotFoundException(TagNotFoundException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(env.getProperty("tag.not.found"), Errors.NOT_FOUND),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleTagFailCreateException(TagFailCreateException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        String.format(env.getProperty("tag.creation.failure"), ex.getMessage()),
                        Errors.FAIL_CREATE), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleTagUpdateException(TagUpdateException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(env.getProperty("tag.update.not.allowed"),
                        Errors.FORBID_UPDATE), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleNameExistsException(NameExistsException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        String.format(env.getProperty("name.exists.error"), ex.getMessage()),
                        Errors.FAIL_CREATE), HttpStatus.BAD_REQUEST);
    }


    /*
     * Certificate exceptions
     */

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleCertificateNotFoundException(CertificateNotFoundException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(env.getProperty("certificate.not.found"), Errors.NOT_FOUND),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleCertificateFailCreateException(CertificateFailCreateException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        String.format(env.getProperty("certificate.creation.failure"), ex.getMessage()),
                        Errors.FAIL_CREATE), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleCertificateFailUpdateException(CertificateFailUpdateException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        String.format(env.getProperty("certificate.update.failure"), ex.getMessage()),
                        Errors.FAIL_CREATE), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleCertificateAddTagException(CertificateAddTagException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        ex.getMessage(), Errors.FAIL_CREATE), HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleServiceException(ServiceException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        ex.getMessage(), Errors.BAD_REQUEST), HttpStatus.BAD_REQUEST
        );
    }

    /*
     * User exceptions
     */

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleUserNotFoundException(UserNotFoundException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(env.getProperty("user.not.found"), Errors.NOT_FOUND),
                HttpStatus.NOT_FOUND);
    }

    /*
     * Order exceptions
     */

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleOrderNotFoundException(OrderNotFoundException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(env.getProperty("order.not.found"), Errors.NOT_FOUND),
                HttpStatus.NOT_FOUND);
    }

    /*
     * Common exceptions
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<ErrorResponse> handleMethodNotAllowedException(HttpRequestMethodNotSupportedException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        ex.getMessage(), Errors.NOT_ALLOWED), HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handleValidationException(MethodArgumentNotValidException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        ex.getMessage(), Errors.BAD_REQUEST), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorResponse> handleBadRequestException(MethodArgumentTypeMismatchException ex) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        "Invalid input data", Errors.BAD_REQUEST), HttpStatus.BAD_REQUEST);
    }

    /**
     * Returns a concatenated string of validation error messages from the provided BindingResult object.
     *
     * @param result the BindingResult object containing field errors
     * @return a string containing error messages for each field error in the BindingResult object
     */

    public static String getErrorMessages(BindingResult result) {
        return result.getFieldErrors().stream()
                .map(e -> "wrong " + e.getField() + ": " + e.getDefaultMessage() + ", ")
                .collect(Collectors.joining());
    }
}