package com.epam.esm.application.controllers;

import com.epam.esm.application.hateoas.LinkManager;
import com.epam.esm.repository.exception.OrderNotFoundException;
import com.epam.esm.service.OrderService;
import com.epam.esm.service.dto.CertificateDTO;
import com.epam.esm.service.dto.OrderDTO;
import com.epam.esm.service.dto.UserDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@ControllerAdvice(basePackages = "com.epam.esm.application.handler")
@RequestMapping("/api/order")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping()
    public ResponseEntity<List<OrderDTO>> getAll(@RequestParam(value = "page", required = false) Integer page,
                                                 @RequestParam(value = "size", required = false) Integer size) {
        List<OrderDTO> list;

        if (page == null || size == null)
            list = orderService.getAll();
        else
            list = orderService.findWithPagination(page, size);

        if (list.isEmpty())
            throw new OrderNotFoundException();

        list.forEach(LinkManager::addLinks);

        return ResponseEntity.ok(list);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDTO> get(@PathVariable("id") Long id) {

        OrderDTO order = orderService.get(id);

        LinkManager.addLinks(order);

        return ResponseEntity.ok(order);
    }


}
