package com.epam.esm.application.controllers;

import com.epam.esm.application.hateoas.LinkManager;
import com.epam.esm.repository.exception.OrderNotFoundException;
import com.epam.esm.repository.exception.UserNotFoundException;
import com.epam.esm.service.OrderService;
import com.epam.esm.service.TagService;
import com.epam.esm.service.UserService;
import com.epam.esm.service.dto.OrderDTO;
import com.epam.esm.service.dto.TagDTO;
import com.epam.esm.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
@ControllerAdvice(basePackages = "com.epam.esm.application.handler")
public class UserController {

    private final UserService userService;
    private final OrderService orderService;
    private final TagService tagService;

    @Autowired
    public UserController
            (
                    UserService userService,
                    OrderService orderService,
                    TagService tagService
            ) {
        this.userService = userService;
        this.orderService = orderService;
        this.tagService = tagService;
    }

    @GetMapping
    public ResponseEntity<List<UserDTO>> getAll(@RequestParam(name = "page", required = false) Integer page,
                                                @RequestParam(name = "size", required = false) Integer size) {
        List<UserDTO> list;

        if (page == null || size == null)
            list = userService.getAll();
        else
            list = userService.findWithPagination(page, size);

        if (list.isEmpty())
            throw new UserNotFoundException();

        list.forEach(LinkManager::addLinks);
        return ResponseEntity.ok(list);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> get(@PathVariable("id") Long id) {

        UserDTO user = userService.get(id);
        LinkManager.addLinks(user);

        return ResponseEntity.ok(user);
    }

    @GetMapping("/{id}/widely_used_tag")
    public ResponseEntity<TagDTO> getWidelyUsedTag(@PathVariable("id") Long id) {

        if (!userService.isExists(id))
            throw new UserNotFoundException();

        TagDTO tag = tagService.getWidelyUsedTag(id);

        LinkManager.addLinks(tag);

        return ResponseEntity.ok(tag);
    }

    @GetMapping("/{userId}/orders")
    public ResponseEntity<List<OrderDTO>> getOrders(@PathVariable("userId") Long userId) {

        List<OrderDTO> orders = orderService.findUsersOrders(userId);

        if (orders.isEmpty())
            throw new OrderNotFoundException();

        orders.forEach(LinkManager::addLinks);

        return ResponseEntity.ok(orders);
    }

    @GetMapping("/{userId}/order/{orderId}")
    public ResponseEntity<OrderDTO> getOrders(@PathVariable("userId") Long userId,
                                              @PathVariable("orderId") Long orderId) {

        OrderDTO order = orderService.findByIdAndUserId(userId, orderId);

        LinkManager.addLinks(order);

        return ResponseEntity.ok(order);
    }

    @PostMapping("/{userId}/certificate/{certificateId}")
    public ResponseEntity<OrderDTO> addOrder(@PathVariable("userId") Long userId,
                                             @PathVariable("certificateId") Long certificateId) {

        OrderDTO order = orderService.addOrder(userId, certificateId);

        LinkManager.addLinks(order);

        return ResponseEntity.ok(order);
    }

}
