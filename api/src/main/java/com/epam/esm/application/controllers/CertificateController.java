package com.epam.esm.application.controllers;

import com.epam.esm.application.handler.ControllerExceptionHandler;
import com.epam.esm.application.hateoas.LinkManager;
import com.epam.esm.repository.exception.CertificateFailCreateException;
import com.epam.esm.repository.exception.CertificateFailUpdateException;
import com.epam.esm.repository.exception.CertificateNotFoundException;
import com.epam.esm.service.CertificateService;
import com.epam.esm.service.dto.CertificateDTO;
import com.epam.esm.service.dto.CertificateUpdateDTO;
import com.epam.esm.service.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
@RequestMapping(value = "/api/certificates", produces = MediaType.APPLICATION_JSON_VALUE)
public class CertificateController {

    private final CertificateService certificateService;

    @Autowired
    public CertificateController(CertificateService certificateService) {
        this.certificateService = certificateService;
    }

    @GetMapping()
    public ResponseEntity<List<CertificateDTO>> getAll(@RequestParam(name = "name", required = false) String name,
                                                       @RequestParam(name = "description", required = false) String description,
                                                       @RequestParam(name = "tag", required = false) String tagName,
                                                       @RequestParam(name = "sortBy", required = false, defaultValue = "id") String sortBy,
                                                       @RequestParam(name = "sortOrder", required = false, defaultValue = "asc") String sortOrder,
                                                       @RequestParam(name = "pageNumber", required = false, defaultValue = "0") Integer pageNumber,
                                                       @RequestParam(name = "pageSize", required = false, defaultValue = "25") Integer pageSize) {

        List<CertificateDTO> certificates = certificateService.findWithPagination(
                pageNumber, pageSize, name, description, tagName, sortBy, sortOrder);

        if (certificates.isEmpty())
            throw new CertificateNotFoundException();

        certificates.forEach(LinkManager::addLinks);

        return ResponseEntity.ok(certificates);
    }

    @GetMapping("/search")
    public ResponseEntity<List<CertificateDTO>> findCertificate(@RequestParam(name = "firstTagName") String firstTagName,
                                                                @RequestParam(name = "secondTagName") String secondTagName) {
        List<CertificateDTO> list = certificateService.findByTwoTags(firstTagName, secondTagName);

        if (list.isEmpty())
            throw new CertificateNotFoundException("Certificates with given criteria not found");

        list.forEach(LinkManager::addLinks);

        return ResponseEntity.ok(list);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CertificateDTO> get(@PathVariable("id") Long id) {
        CertificateDTO certificate = certificateService.get(id);

        LinkManager.addLinks(certificate);

        return ResponseEntity.ok(certificate);
    }


    @PostMapping()
    public ResponseEntity<CertificateDTO> add(@Validated @RequestBody CertificateDTO certificateDTO,
                                              BindingResult result) {

        if (result.hasErrors())
            throw new CertificateFailCreateException(
                    ControllerExceptionHandler.getErrorMessages(result));

        return ResponseEntity.ok(certificateService.add(certificateDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") Long id) {
        if (!certificateService.isExists(id))
            throw new CertificateNotFoundException();

        certificateService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<CertificateDTO> update(@PathVariable("id") Long id,
                                                 @Validated @RequestBody CertificateUpdateDTO certificateUpdateDTO,
                                                 BindingResult result) {
        if (result.hasErrors())
            throw new CertificateFailUpdateException(
                    ControllerExceptionHandler.getErrorMessages(result));

        if (certificateUpdateDTO == null)
            throw new ServiceException("Wrong input data");
        CertificateDTO updated = certificateService.update(id, certificateUpdateDTO);
        LinkManager.addLinks(updated);
        return ResponseEntity.ok(updated);
    }


    @PostMapping("/{certificateId}/tags/{tagId}")
    public ResponseEntity<HttpStatus> addTagToCertificate(@PathVariable Long certificateId,
                                                          @PathVariable Long tagId) {
        certificateService.addTag(certificateId, tagId);
        return ResponseEntity.ok(HttpStatus.OK);
    }


    @DeleteMapping("/{certificateId}/tags/{tagId}")
    public ResponseEntity<HttpStatus> deleteTag(@PathVariable Long certificateId,
                                                @PathVariable Long tagId) {
        certificateService.deleteTag(certificateId, tagId);
        return ResponseEntity.ok(HttpStatus.OK);
    }

}