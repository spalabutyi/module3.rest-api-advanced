package com.epam.esm.application.controllers;

import com.epam.esm.application.handler.ControllerExceptionHandler;
import com.epam.esm.application.hateoas.LinkManager;
import com.epam.esm.repository.exception.TagFailCreateException;
import com.epam.esm.repository.exception.TagNotFoundException;
import com.epam.esm.repository.exception.TagUpdateException;
import com.epam.esm.service.TagService;
import com.epam.esm.service.dto.TagDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/tags", produces = MediaType.APPLICATION_JSON_VALUE)
@RestControllerAdvice(basePackages = "com.epam.esm.application.handler")
public class TagController {

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping()
    public ResponseEntity<List<TagDTO>> getAll(@RequestParam(value = "page", required = false) Integer page,
                                               @RequestParam(value = "size", required = false) Integer size) {
        List<TagDTO> list;

        if (page == null || size == null)
            list = tagService.getAll();
        else
            list = tagService.findWithPagination(page, size);

        if (list.isEmpty())
            throw new TagNotFoundException();

        list.forEach(LinkManager::addLinks);
        return ResponseEntity.ok(list);
    }


    @GetMapping("/{id}")
    public ResponseEntity<TagDTO> get(@PathVariable("id") Long id) {
        TagDTO tag = tagService.get(id);
        LinkManager.addLinks(tag);
        return ResponseEntity.ok(tag);
    }


    @PostMapping()
    public ResponseEntity<TagDTO> add(@RequestBody @Validated TagDTO tagDTO,
                                      BindingResult result) {
        if (result.hasErrors())
            throw new TagFailCreateException(
                    ControllerExceptionHandler.getErrorMessages(result));

        return ResponseEntity.ok(tagService.add(tagDTO));
    }

    @PatchMapping()
    public ResponseEntity<Void> update() {
        throw new TagUpdateException();
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") long id) {
        if (!tagService.isExists(id)) throw new TagNotFoundException();

        tagService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}