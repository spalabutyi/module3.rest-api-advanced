package com.epam.esm.repository;

import com.epam.esm.model.Certificate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CertificateRepository extends JpaRepository<Certificate, Long> {

    /**
     * Returns an optional Certificate with the given name.
     *
     * @param name the name of the Certificate to find
     * @return an Optional object containing the Certificate if found, otherwise an empty Optional
     */
    Optional<Certificate> findByName(String name);

    /**
     * Returns a page of Certificates whose name contains the given substring.
     *
     * @param name     the substring to search for in Certificate names
     * @param pageable the pagination information
     * @return a Page object containing Certificates whose names contain the given substring
     */
    Page<Certificate> findByNameContaining(String name, Pageable pageable);

    /**
     * Returns a page of Certificates whose name and description contain the given substrings.
     *
     * @param name        the substring to search for in Certificate names
     * @param description the substring to search for in Certificate descriptions
     * @param pageable    the pagination information
     * @return a Page object containing Certificates whose names and descriptions contain the given substrings
     */
    Page<Certificate> findByNameContainingAndDescriptionContaining(String name, String description, Pageable pageable);

    /**
     * Returns a page of Certificates whose description contains the given substring.
     *
     * @param description the substring to search for in Certificate descriptions
     * @param pageable    the pagination information
     * @return a Page object containing Certificates whose descriptions contain the given substring
     */
    Page<Certificate> findByDescriptionContaining(String description, Pageable pageable);

    /**
     * Returns a list of Certificates that have both the given tags.
     *
     * @param firstTagName  the name of the first tag to search for
     * @param secondTagName the name of the second tag to search for
     * @return a List of Certificates that have both the given tags
     */
    @Query("SELECT c FROM Certificate c JOIN c.tags t1 JOIN c.tags t2 WHERE t1.name = :firstTagName AND t2.name = :secondTagName")
    List<Certificate> findByTags(@Param("firstTagName") String firstTagName,
                                 @Param("secondTagName") String secondTagName);

    /**
     * Returns a page of Certificates that have the given tag.
     *
     * @param tagName  the name of the tag to search for
     * @param pageable the pagination information
     * @return a Page object containing Certificates that have the given tag
     */
    @Query("SELECT c FROM Certificate c JOIN c.tags t WHERE t.name = :tagName")
    Page<Certificate> findByTagName(@Param("tagName") String tagName, Pageable pageable);
}