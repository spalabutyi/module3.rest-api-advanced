package com.epam.esm.repository;

import com.epam.esm.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

    /**
     * Returns an Optional object containing the tag with the specified name,
     * or an empty Optional if no such tag exists.
     *
     * @param name the name of the tag to search for
     * @return an Optional object containing the tag with the specified name,
     * or an empty Optional if no such tag exists
     */
    Optional<Tag> findByName(String name);

}
