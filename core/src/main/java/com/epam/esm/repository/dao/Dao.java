package com.epam.esm.repository.dao;

import com.epam.esm.model.BaseEntity;

import java.util.List;

/**
 * The DAO interface represents common CRUD operations for the database entities.
 *
 * @param <T> the type of entity.
 */
public interface Dao<T extends BaseEntity> {

    /**
     * Adds a new entity to the data source.
     *
     * @param t The entity to add.
     * @return The added entity.
     */
    T add(T t);

    /**
     * Updates an existing entity in the data source.
     *
     * @param t The entity to update.
     * @return True if the entity was updated successfully, false otherwise.
     */
    boolean update(T t);

    /**
     * Deletes an existing entity from the data source by its ID.
     *
     * @param id The ID of the entity to delete.
     * @return True if the entity was deleted successfully, false otherwise.
     */
    boolean delete(Long id);

    /**
     * Retrieves an existing entity from the data source by its ID.
     *
     * @param id The ID of the entity to retrieve.
     * @return The retrieved entity, or null if the entity was not found.
     */
    T get(Long id);

    /**
     * Retrieves all entities from the data source.
     *
     * @return A list of all retrieved entities.
     */
    List<T> getAll();

    /**
     * Checks if an entity with the given ID exists in the data source.
     *
     * @param id The ID of the entity to check.
     * @return True if an entity with the given ID exists, false otherwise.
     */
    boolean isExists(Long id);

    /**
     * Checks if an entity with the given name exists in the data source.
     *
     * @param name The name of the entity to check.
     * @return True if an entity with the given name exists, false otherwise.
     */
    boolean isExists(String name);
}