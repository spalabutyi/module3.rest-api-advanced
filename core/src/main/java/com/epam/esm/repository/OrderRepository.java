package com.epam.esm.repository;

import com.epam.esm.model.Order;
import com.epam.esm.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    /**
     * Finds an order by ID and user.
     *
     * @param id   the ID of the order
     * @param user the user associated with the order
     * @return an optional containing the order, or empty if not found
     */
    Optional<Order> findByIdAndUser(Long id, User user);

    /**
     * Finds an order by user ID and certificate ID.
     *
     * @param userId        the ID of the user associated with the order
     * @param certificateId the ID of the certificate associated with the order
     * @return an optional containing the order, or empty if not found
     */
    Optional<Order> findOrderByUserIdAndCertificateId(Long userId, Long certificateId);

    /**
     * Finds all orders for a given user.
     *
     * @param userId the ID of the user to find orders for
     * @return a list of orders for the user
     */
    List<Order> findOrdersByUserId(Long userId);
}
