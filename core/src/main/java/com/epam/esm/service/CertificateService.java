package com.epam.esm.service;

import com.epam.esm.service.dto.CertificateDTO;
import com.epam.esm.service.dto.CertificateUpdateDTO;

import java.util.List;

/**
 * The CertificateService interface extends the Service interface for handling operations related to certificates.
 */
public interface CertificateService extends Service<CertificateDTO> {

    CertificateDTO update(Long id, CertificateUpdateDTO certificateUpdateDTO);

    /**
     * Checks if the given certificate has the given tag.
     *
     * @param certificateId The ID of the certificate to check.
     * @param tagId         The ID of the tag to check for.
     * @return true if the certificate has the tag, false otherwise.
     */
    boolean hasTags(Long certificateId, Long tagId);

    /**
     * Adds a tag to the given certificate.
     *
     * @param certificateId The ID of the certificate to add the tag to.
     * @param tagId         The ID of the tag to add.
     * @return true if the tag was added successfully, false otherwise.
     */
    boolean addTag(Long certificateId, Long tagId);

    /**
     * Deletes a tag from the given certificate.
     *
     * @param certificateId The ID of the certificate to delete the tag from.
     * @param tagId         The ID of the tag to delete.
     * @return true if the tag was deleted successfully, false otherwise.
     */
    boolean deleteTag(Long certificateId, Long tagId);

    /**
     * Finds a list of certificates with pagination based on specified parameters.
     *
     * @param page        the page number
     * @param size        the page size
     * @param name        the certificate name to search for
     * @param description the certificate description to search for
     * @param tagName     the tag name to search for
     * @param sortBy      the field to sort the results by
     * @param order       the order to sort the results in
     * @return a list of CertificateDTO objects matching the search criteria
     */
    List<CertificateDTO> findWithPagination(Integer page, Integer size,
                                            String name, String description, String tagName,
                                            String sortBy, String order);

    /**
     * Finds a list of certificates based on two tag names.
     *
     * @param firstTagName  the name of the first tag
     * @param secondTagName the name of the second tag
     * @return a list of CertificateDTO objects that have both tags
     */
    List<CertificateDTO> findByTwoTags(String firstTagName, String secondTagName);

}