package com.epam.esm.service.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.springframework.stereotype.Component;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"name", "email"}, callSuper = false)
@ToString
@Component
public class UserDTO extends AbstractDTO {

    @NotBlank
    @Size(min = 1, max = 32, message = "Name must be between 1 and 32 characters")
    private String name;

    @Email
    private String email;

    @JsonBackReference
    private Set<OrderDTO> orders;

    public UserDTO(String name, String email) {
        this.name = name;
        this.email = email;
    }
}