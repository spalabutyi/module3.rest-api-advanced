package com.epam.esm.service.impl;

import com.epam.esm.model.User;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.repository.exception.UserNotFoundException;
import com.epam.esm.repository.exception.UserOperationNotSupportedException;
import com.epam.esm.service.UserService;
import com.epam.esm.service.dto.UserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public UserDTO add(UserDTO userDTO) {
        throw new UserOperationNotSupportedException("Unable to add a new user. Operation is not supported");
    }

    @Override
    public UserDTO update(UserDTO userDTO) {
        throw new UserOperationNotSupportedException("Unable to update user. Operation not supported");
    }

    @Override
    public boolean delete(Long id) {
        throw new UserOperationNotSupportedException("Unable to delete user. Operation not supported");
    }

    @Override
    public UserDTO get(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) throw new UserNotFoundException();
        return modelMapper.map(user, UserDTO.class);
    }

    @Override
    public List<UserDTO> getAll() {
        return userRepository.findAll().stream()
                .map(user -> modelMapper.map(user, UserDTO.class))
                .toList();
    }

    @Override
    public boolean isExists(Long id) {
        return userRepository.existsById(id);
    }

    @Override
    public boolean isExists(String name) {
        return false;
    }

    @Override
    public List<UserDTO> findWithPagination(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return userRepository.findAll(pageable).getContent().stream()
                .map(user -> modelMapper.map(user, UserDTO.class))
                .toList();
    }

}
