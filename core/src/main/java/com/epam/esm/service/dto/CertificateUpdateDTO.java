package com.epam.esm.service.dto;

import jakarta.validation.constraints.*;
import lombok.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Setter
@Getter
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"name", "description", "price", "duration"}, callSuper = false)
@Component
public class CertificateUpdateDTO extends AbstractDTO {

    @Size(min = 1, max = 32, message = "Name must be between 1 and 32 characters")
    private String name;

    @Size(min = 1, max = 256, message = "Description must be between 1 and 256 characters")
    private String description;

    @Positive
    @DecimalMin(value = "0.01", message = "Price must be greater than or equal to 0.01")
    @Digits(integer = 9, fraction = 2, message = "Pattern: xxxxxxxxx.yy")
    private BigDecimal price;

    @Min(value = 1, message = "Duration must be at least 1 day")
    @Max(value = 365, message = "Duration cannot be more than 365 days")
    private Integer duration;

}