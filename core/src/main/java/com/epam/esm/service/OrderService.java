package com.epam.esm.service;

import com.epam.esm.service.dto.OrderDTO;

import java.util.List;

public interface OrderService extends Service<OrderDTO> {

    /**
     * Finds a page of orders with the specified page number and size.
     *
     * @param page the page number
     * @param size the page size
     * @return a list of OrderDTO objects for the requested page
     */
    List<OrderDTO> findWithPagination(Integer page, Integer size);

    /**
     * Finds all orders for a given user.
     *
     * @param userId the ID of the user to find orders for
     * @return a list of OrderDTO objects for the user
     */
    List<OrderDTO> findUsersOrders(Long userId);

    /**
     * Finds an order by its ID and user ID.
     *
     * @param userId  the ID of the user who owns the order
     * @param orderId the ID of the order to find
     * @return the OrderDTO object for the specified order ID and user ID, or null if not found
     */
    OrderDTO findByIdAndUserId(Long userId, Long orderId);

    /**
     * Checks if an order exists for the specified user and certificate.
     *
     * @param userId        the ID of the user who owns the order
     * @param certificateId the ID of the certificate to check for
     * @return true if the user has an order for the certificate, false otherwise
     */
    boolean checkOrder(Long userId, Long certificateId);

    /**
     * Adds a new order for the specified user and certificate.
     *
     * @param userId        the ID of the user who is placing the order
     * @param certificateId the ID of the certificate being ordered
     * @return the OrderDTO object for the newly created order
     */
    OrderDTO addOrder(Long userId, Long certificateId);

}