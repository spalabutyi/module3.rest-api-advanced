package com.epam.esm.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"user", "certificate", "price", "purchaseDate"}, callSuper = false)
@ToString
@Component
public class OrderDTO extends AbstractDTO {

    @NotNull
    @JsonManagedReference
    private UserDTO user;

    @NotNull
    private CertificateDTO certificate;

    @Positive
    @DecimalMin(value = "0.01", message = "Price must be greater than or equal to 0.01")
    @Digits(integer = 9, fraction = 2, message = "Pattern: xxxxxxxxx.yy")
    private BigDecimal price;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime purchaseDate;

    public OrderDTO(UserDTO user, CertificateDTO certificate, BigDecimal price, LocalDateTime purchaseDate) {
        this.user = user;
        this.certificate = certificate;
        this.price = price;
        this.purchaseDate = purchaseDate;
    }

}