package com.epam.esm.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(of = {"name", "email"}, callSuper = false)
@ToString()
@Slf4j
@Entity
@Table(name = "users")
public class User extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<Order> orders;

    @PrePersist
    public void onPrePersist() {
        log.info("{} added", this);
    }

    @PreUpdate
    public void onPreUpdate() {
        log.info("{} updated", this);
    }

    @PreRemove
    public void onPreRemove() {
        log.info("{} removed", this);
    }
}