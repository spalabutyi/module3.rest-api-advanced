package com.epam.esm.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"name", "description", "price", "duration", "createDate", "lastUpdateDate"}, callSuper = false)
@ToString(of = {"name", "description", "price", "duration", "createDate", "lastUpdateDate"})
@Slf4j
@Entity
@Table(name = "gift_certificate")
public class Certificate extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @Column(name = "last_update_date")
    private LocalDateTime lastUpdateDate;


    @ManyToMany
    @JoinTable(
            name = "gift_certificate_tag",
            joinColumns = @JoinColumn(name = "gift_certificate_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags;

    @PrePersist
    public void onPrePersist() {
        log.info("{} added", this);
    }

    @PreUpdate
    public void onPreUpdate() {
        log.info("{} updated", this);
    }

    @PreRemove
    public void onPreRemove() {
        log.info("{} removed", this);
    }

}