package com.epam.esm.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString()
@Slf4j
@Entity
@Table(name = "orders")
public class Order extends BaseEntity {

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "purchase_date")
    private LocalDateTime purchaseDate;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "certificate_id", referencedColumnName = "id")
    private Certificate certificate;

    @PrePersist
    public void onPrePersist() {
        log.info("{} added", this);
    }

    @PreUpdate
    public void onPreUpdate() {
        log.info("{} updated", this);
    }

    @PreRemove
    public void onPreRemove() {
        log.info("{} removed", this);
    }

}