package com.epam.esm.service.impl;

import com.epam.esm.config.MapperConfig;
import com.epam.esm.config.TestConfig;
import com.epam.esm.model.Tag;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.dao.TagDAO;
import com.epam.esm.repository.exception.TagFailCreateException;
import com.epam.esm.service.TagService;
import com.epam.esm.service.dto.TagDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@Profile("test")
@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = {TestConfig.class, MapperConfig.class})
class TagServiceImplTest {

    @Mock
    private TagRepository tagRepository;

    @Mock
    private TagDAO tagDAO;

    @Autowired
    private ModelMapper modelMapper;

    private TagService tagService;

    private final Tag testTag = new Tag(1L, "Tag 1");

    @BeforeEach
    void setUp() {
        tagService = new TagServiceImpl(tagRepository, tagDAO, modelMapper);
    }

    @Test
    void getAllTags_test() {

        List<Tag> list = new ArrayList<>();
        list.add(new Tag(2L, "Tag 2"));
        list.add(new Tag(3L, "Tag 3"));
        list.add(new Tag(4L, "Tag 4"));

        when(tagRepository.findAll()).thenReturn(list);

        List<TagDTO> all = tagService.getAll();

        verify(tagRepository).findAll();
        assertEquals(3, all.size());
    }

    @Test
    void getValidTag_success_test() {
        String actual = "Tag 1";

        when(tagRepository.findById(anyLong())).thenReturn(Optional.of(testTag));
        assertEquals(tagService.get(1L).getName(), actual);
        verify(tagRepository).findById(1L);
    }

    @Test
    void addTag_success_test() {

        when(tagRepository.findByName(anyString()))
                .thenReturn(Optional.empty());
        when(tagRepository.save(any(Tag.class)))
                .thenReturn(testTag);

        TagDTO added = tagService.add(modelMapper.map(testTag, TagDTO.class));
        assertNotNull(added);
        verify(tagRepository).findByName(anyString());
        verify(tagRepository).save(any(Tag.class));
    }

    @Test
    void addTag_fail_test() {
        when(tagRepository.findByName(anyString()))
                .thenReturn(Optional.of(testTag));
        TagDTO mapped = modelMapper.map(testTag, TagDTO.class);

        assertThrows(TagFailCreateException.class,
                () -> tagService.add(mapped)
        );
    }

    @Test
    void deleteTag_success_test() {
        Long id = testTag.getId();
        tagService.delete(id);

        verify(tagRepository).deleteById(id);
    }

    @Test
    void isExists_success_test() {
        when(tagRepository.existsById(anyLong())).thenReturn(true);

        boolean idExists = tagService.isExists(1L);

        verify(tagRepository).existsById(anyLong());
        assertTrue(idExists);
    }

    @Test
    void findWithPagination_test() {

        int page = 0;
        int size = 5;

        List<Tag> list = new ArrayList<>();
        list.add(new Tag(2L, "Tag 2"));
        list.add(new Tag(3L, "Tag 3"));
        list.add(new Tag(4L, "Tag 4"));

        Page<Tag> pageResult = new PageImpl<>(list);

        when(tagRepository.findAll(any(Pageable.class)))
                .thenReturn(pageResult);

        tagService.findWithPagination(page, size);

        verify(tagRepository, times(1))
                .findAll(any(Pageable.class));

        Assertions.assertEquals(3, list.size());
    }

    @Test
    void updateTest() {
        assertThrows(UnsupportedOperationException.class, () ->
                tagService.update(modelMapper.map(testTag, TagDTO.class)));
    }

}