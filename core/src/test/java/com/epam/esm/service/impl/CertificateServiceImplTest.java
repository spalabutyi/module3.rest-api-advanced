package com.epam.esm.service.impl;

import com.epam.esm.config.MapperConfig;
import com.epam.esm.config.TestConfig;
import com.epam.esm.model.Certificate;
import com.epam.esm.model.Tag;
import com.epam.esm.repository.CertificateRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.exception.CertificateAddTagException;
import com.epam.esm.repository.exception.CertificateNotFoundException;
import com.epam.esm.repository.exception.TagFailCreateException;
import com.epam.esm.repository.exception.TagNotFoundException;
import com.epam.esm.service.CertificateService;
import com.epam.esm.service.dto.CertificateDTO;
import com.epam.esm.service.dto.CertificateUpdateDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@Profile("test")
@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = {TestConfig.class, MapperConfig.class})
class CertificateServiceImplTest {

    @Mock
    private CertificateRepository certificateRepository;

    @Mock
    private TagRepository tagRepository;

    @Autowired
    private ModelMapper modelMapper;

    private CertificateService certificateService;

    private static List<CertificateDTO> testList;

    private final Certificate testCertificate = new Certificate("Name 5", "Description 5", BigDecimal.valueOf(55), 250,
            LocalDateTime.now(), LocalDateTime.now(), new HashSet<>());


    @BeforeAll
    static void beforeAll() {
        testList = List.of(
                new CertificateDTO("Name 1", "Description 1", BigDecimal.valueOf(99), 50),
                new CertificateDTO("Name 2", "Description 2", BigDecimal.valueOf(88), 100),
                new CertificateDTO("Name 3", "Description 3", BigDecimal.valueOf(77), 150),
                new CertificateDTO("Name 4", "Description 4", BigDecimal.valueOf(66), 200),
                new CertificateDTO("Name 5", "Description 5", BigDecimal.valueOf(55), 250)
        );
    }

    @BeforeEach
    void setUp() {
        certificateService = new CertificateServiceImpl(certificateRepository, tagRepository, modelMapper);
    }

    @Test
    void getAllCertificates_success_test() {
        List<Certificate> list = new ArrayList<>();
        testList.forEach(e -> list.add(modelMapper.map(e, Certificate.class)));

        when(certificateRepository.findAll()).thenReturn(list);
        assertEquals(5, certificateService.getAll().size());
        verify(certificateRepository).findAll();
    }

    @Test
    void getValidCertificates_success_test() {
        Certificate certificate = modelMapper.map(testList.get(0), Certificate.class);

        when(certificateRepository.findById(anyLong())).thenReturn(Optional.ofNullable(certificate));
        assertEquals(certificateService.get(0L).getName(), "Name 1");
        verify(certificateRepository).findById(0L);
    }

    @Test
    void addCertificate_success_test() {
        testCertificate.setId(0L);
        when(certificateRepository.findByName(anyString()))
                .thenReturn(Optional.empty());
        when(certificateRepository.save(any(Certificate.class)))
                .thenReturn(testCertificate);

        CertificateDTO added = certificateService.add(modelMapper.map(testCertificate, CertificateDTO.class));
        assertNotNull(added);
        verify(certificateRepository).findByName(anyString());
        verify(certificateRepository).save(any(Certificate.class));
    }

    @Test
    void addCertificate_fail_test() {
        when(certificateRepository.findByName(anyString()))
                .thenReturn(Optional.of(testCertificate));
        CertificateDTO mapped = modelMapper.map(testCertificate, CertificateDTO.class);

        assertThrows(TagFailCreateException.class,
                () -> certificateService.add(mapped)
        );
    }

    @Test
    void deleteCertificate_success_test() {
        Long id = testCertificate.getId();
        certificateService.delete(id);

        verify(certificateRepository).deleteById(id);
    }

    @Test
    void isExists_success_test() {

        when(certificateRepository.findByName(anyString())).thenReturn(Optional.of(testCertificate));
        when(certificateRepository.existsById(anyLong())).thenReturn(false);

        boolean nameExists = certificateService.isExists("Name 5");
        boolean idExists = certificateService.isExists(1L);

        verify(certificateRepository).findByName(anyString());
        assertTrue(nameExists);
        verify(certificateRepository).existsById(anyLong());
        assertFalse(idExists);
    }

    @Test
    void updateCertificate_success_test() {

        when(certificateRepository.save(any(Certificate.class)))
                .thenReturn(testCertificate);

        CertificateDTO mapped = modelMapper.map(testCertificate, CertificateDTO.class);
        certificateService.update(mapped);

        assertEquals(testCertificate.getName(), mapped.getName());
        verify(certificateRepository).save(any(Certificate.class));
    }

    @Test
    void updateByCertificateId_success_test() {

        CertificateUpdateDTO mapped = modelMapper.map(testCertificate, CertificateUpdateDTO.class);
        mapped.setId(0L);
        Certificate certificate = modelMapper.map(testList.get(0), Certificate.class);

        when(certificateRepository.findById(anyLong())).thenReturn(Optional.ofNullable(certificate));
        when(certificateRepository.save(any(Certificate.class)))
                .thenReturn(testCertificate);

        CertificateDTO updated = certificateService.update(0L, mapped);

        assertEquals(testCertificate.getName(), mapped.getName());
        assertNotNull(updated);
        verify(certificateRepository).save(any(Certificate.class));
    }

    @Test
    void updateByCertificateId_fail_test() {

        CertificateUpdateDTO mapped = modelMapper.map(testCertificate, CertificateUpdateDTO.class);
        mapped.setId(0L);

        when(certificateRepository.findById(anyLong()))
                .thenReturn(Optional.empty());

        assertThrows(CertificateNotFoundException.class,
                () -> certificateService.update(0L, mapped));
    }

    @Test
    void certificateHasTag_success_test() {
        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("Test tag 1");

        testCertificate.setId(1L);
        testCertificate.setTags(Set.of(tag));

        when(certificateRepository.findById(anyLong()))
                .thenReturn(Optional.of(testCertificate));

        assertAll(
                () -> assertTrue(certificateService.hasTags(1L, 1L)),
                () -> assertFalse(certificateService.hasTags(1L, 2L))
        );
    }

    @Test
    void certificateAddTag_success_test() {
        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("Test tag 1");
        Tag tag2 = new Tag();
        tag2.setId(2L);
        tag2.setName("Test tag 2");

        testCertificate.setId(1L);
        Set<Tag> set = new HashSet<>();
        set.add(tag1);
        testCertificate.setTags(set);

        when(certificateRepository.existsById(anyLong()))
                .thenReturn(true);
        when(tagRepository.existsById(anyLong()))
                .thenReturn(true);
        when(tagRepository.findById(anyLong()))
                .thenReturn(Optional.of(tag2));
        when(certificateRepository.findById(anyLong()))
                .thenReturn(Optional.of(testCertificate));

        assertEquals(1, testCertificate.getTags().size());
        boolean addTag = certificateService.addTag(1L, 2L);
        assertTrue(addTag);
        assertEquals(2, testCertificate.getTags().size());
    }

    @Test
    void certificateAddTag_DeleteTag_nonExistedCertificate_fail_test() {
        when(certificateRepository.existsById(anyLong()))
                .thenReturn(false);
        assertAll(
                () -> assertThrows(CertificateNotFoundException.class,
                        () -> certificateService.addTag(1L, 1L)),
                () -> assertThrows(CertificateNotFoundException.class,
                        () -> certificateService.deleteTag(1L, 1L))
        );
    }

    @Test
    void certificateAddTag_DeleteTag_nonExistedTag_test() {
        when(certificateRepository.existsById(anyLong()))
                .thenReturn(true);
        when(tagRepository.existsById(anyLong()))
                .thenReturn(false);
        assertAll(
                () -> assertThrows(TagNotFoundException.class,
                        () -> certificateService.addTag(1L, 1L)),
                () -> assertThrows(TagNotFoundException.class,
                        () -> certificateService.deleteTag(1L, 1L))
        );
    }

    @Test
    void certificateAddTag_alreadyAdded_tag_test() {
        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("Test tag 1");

        testCertificate.setId(1L);
        testCertificate.setTags(Set.of(tag));

        when(certificateRepository.existsById(anyLong()))
                .thenReturn(true);
        when(tagRepository.existsById(anyLong()))
                .thenReturn(true);
        when(certificateRepository.findById(anyLong()))
                .thenReturn(Optional.of(testCertificate));

        assertThrows(CertificateAddTagException.class,
                () -> certificateService.addTag(1L, 1L));
    }

    @Test
    void certificateDeleteTag_success() {
        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("Test tag 1");
        Tag tag2 = new Tag();
        tag2.setId(2L);
        tag2.setName("Test tag 2");

        testCertificate.setId(1L);
        Set<Tag> set = new HashSet<>();
        set.add(tag1);
        set.add(tag2);
        testCertificate.setTags(set);

        when(certificateRepository.existsById(anyLong()))
                .thenReturn(true);
        when(tagRepository.existsById(anyLong()))
                .thenReturn(true);
        when(tagRepository.findById(anyLong()))
                .thenReturn(Optional.of(tag2));
        when(certificateRepository.findById(anyLong()))
                .thenReturn(Optional.of(testCertificate));

        assertEquals(2, testCertificate.getTags().size());
        boolean addTag = certificateService.deleteTag(1L, 2L);
        assertTrue(addTag);
        assertEquals(1, testCertificate.getTags().size());
    }

    @Test
    void findWithPaginationAndSorting_test1() {
        int page = 0;
        int size = 5;
        String name = "Name 1";
        String description = "Description";
        String tagName = "Tag name";
        String sortBy = "name";
        String sortOrder = "asc";

        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, sortBy));

        List<Certificate> certificates = new ArrayList<>();
        testList.forEach(e -> certificates.add(modelMapper.map(e, Certificate.class)));

        Page<Certificate> pageCertificates = new PageImpl<>(certificates, pageable, certificates.size());

        when(certificateRepository.findByNameContaining(name, pageable)).thenReturn(pageCertificates);

        List<CertificateDTO> result = certificateService.findWithPagination(page, size, name, null, null, sortBy, sortOrder);

        assertEquals(5, result.size());
        assertEquals("Name 1", result.get(0).getName());
        assertEquals("Description 1", result.get(0).getDescription());
        assertEquals("Name 2", result.get(1).getName());
        assertEquals("Description 2", result.get(1).getDescription());

    }

    @Test
    void findWithPaginationAndSorting_test2() {
        Integer page = 0;
        Integer size = 5;
        String name = "certificate";
        String description = "description";
        String sortBy = "name";
        String sortOrder = "asc";

        List<Certificate> certificateList = new ArrayList<>();
        Certificate certificate1 = new Certificate();
        certificate1.setId(1L);
        certificate1.setName("certificate1");
        Certificate certificate2 = new Certificate();
        certificate2.setId(2L);
        certificate2.setName("certificate2");
        certificateList.add(certificate1);
        certificateList.add(certificate2);

        Page<Certificate> pageResult = new PageImpl<>(certificateList);

        when(certificateRepository.findByNameContainingAndDescriptionContaining(anyString(), anyString(), any(Pageable.class)))
                .thenReturn(pageResult);

        List<CertificateDTO> result = certificateService.findWithPagination(page, size, name, description, null, sortBy, sortOrder);

        verify(certificateRepository, times(1))
                .findByNameContainingAndDescriptionContaining(anyString(), anyString(), any(Pageable.class));

        Assertions.assertEquals(2, result.size());
    }

    @Test
    void findWithPaginationAndSorting_test3() {
        Integer page = 0;
        Integer size = 5;
        String name = "certificate";
        String description = "description";
        String sortBy = "name";
        String sortOrder = "asc";

        List<Certificate> certificateList = new ArrayList<>();
        Certificate certificate1 = new Certificate();
        certificate1.setId(1L);
        certificate1.setName("certificate1");
        Certificate certificate2 = new Certificate();
        certificate2.setId(2L);
        certificate2.setName("certificate2");
        certificateList.add(certificate1);
        certificateList.add(certificate2);

        Page<Certificate> pageResult = new PageImpl<>(certificateList);

        when(certificateRepository.findByDescriptionContaining(anyString(), any(Pageable.class)))
                .thenReturn(pageResult);

        List<CertificateDTO> result = certificateService.findWithPagination(page, size, null, description, null, sortBy, sortOrder);

        verify(certificateRepository, times(1))
                .findByDescriptionContaining(anyString(), any(Pageable.class));

        Assertions.assertEquals(2, result.size());
    }

    @Test
    void findWithPaginationAndSorting_test4() {
        Integer page = 0;
        Integer size = 5;
        String name = "certificate";
        String description = "description";
        String sortBy = "name";
        String sortOrder = "asc";

        List<Certificate> certificateList = new ArrayList<>();
        Certificate certificate1 = new Certificate();
        certificate1.setId(1L);
        certificate1.setName("certificate1");
        Certificate certificate2 = new Certificate();
        certificate2.setId(2L);
        certificate2.setName("certificate2");
        certificateList.add(certificate1);
        certificateList.add(certificate2);

        Page<Certificate> pageResult = new PageImpl<>(certificateList);

        when(certificateRepository.findByTagName(anyString(), any(Pageable.class)))
                .thenReturn(pageResult);

        List<CertificateDTO> result = certificateService.findWithPagination(page, size, null, null, "Tag Name", sortBy, sortOrder);

        verify(certificateRepository, times(1))
                .findByTagName(anyString(), any(Pageable.class));

        Assertions.assertEquals(2, result.size());
    }

    @Test
    void findWithPaginationAndSorting_test5() {
        Integer page = 0;
        Integer size = 5;
        String name = "certificate";
        String description = "description";
        String sortBy = "name";
        String sortOrder = "asc";

        List<Certificate> certificateList = new ArrayList<>();
        Certificate certificate1 = new Certificate();
        certificate1.setId(1L);
        certificate1.setName("certificate1");
        Certificate certificate2 = new Certificate();
        certificate2.setId(2L);
        certificate2.setName("certificate2");
        certificateList.add(certificate1);
        certificateList.add(certificate2);

        Page<Certificate> pageResult = new PageImpl<>(certificateList);

        when(certificateRepository.findAll(any(Pageable.class)))
                .thenReturn(pageResult);

        List<CertificateDTO> result = certificateService.findWithPagination(page, size, null, null, null, sortBy, sortOrder);

        verify(certificateRepository, times(1))
                .findAll(any(Pageable.class));

        Assertions.assertEquals(2, result.size());
    }

    @Test
    void findCertificateByTwoTags_success_test() {
        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("Test tag 1");
        Tag tag2 = new Tag();
        tag2.setId(2L);
        tag2.setName("Test tag 2");

        testCertificate.setId(1L);
        Set<Tag> set = new HashSet<>();
        set.add(tag1);
        set.add(tag2);
        testCertificate.setTags(set);

        when(certificateRepository.findByTags(anyString(), anyString()))
                .thenReturn(List.of(testCertificate));

        List<CertificateDTO> byTwoTags = certificateService.findByTwoTags("Test tag 1", "Test tag 2");
        assertEquals(byTwoTags.get(0).getTags().size(), 2);
    }

}